import { BadRequestException, Injectable } from '@nestjs/common';
import axios from 'axios';
import { song } from './dtos/song.dto';
import * as fs from 'fs';

@Injectable()
export class AppService {
  getHello(): string {
    return 'Hello World!';
  }

  async getItunesMusic(artistName: string): Promise<any> {
    return axios
      .get('https://itunes.apple.com/search?term=' + artistName, {})
      .then((response: any) => {
        const data = response.data.results
          .filter(
            (item: any) =>
              item.artistName.toLowerCase() === artistName.toLowerCase(),
          )
          .slice(0, 25);

        let albumes = data.map((item: any) => item.collectionName);
        albumes = albumes.filter((item: any, index: any) => {
          return albumes.indexOf(item) === index;
        });

        const result = {
          total_albumes: albumes.length,
          total_canciones: data.map((item: any) => item.trackName).length,
          albumes: albumes,
          canciones: data,
        };

        return result;
      })
      .catch((error: any) => error);
  }

  async addToFavorites(cancion: song): Promise<string> {
    return axios
      .get('https://itunes.apple.com/search?term=' + cancion.nombre_banda, {})
      .then((response: any) => {
        const data = response.data.results.filter(
          (item: any) =>
            item.artistName.toLowerCase() ===
            cancion.nombre_banda.toLowerCase(),
        );

        const result = data.filter(
          (item: any) => item.trackId == cancion.cancion_id,
        );

        if (result.length > 0) {
          const filename = 'favorities.json';
          const songJson = JSON.stringify(cancion);
          fs.writeFileSync(filename, songJson);
          return songJson;
        } else {
          throw new BadRequestException('Canción Inválida');
        }
      })
      .catch((error: any) => error);
  }
}
