FROM node:16.16-alpine

WORKDIR /app
COPY . .

RUN npm install @nestjs/cli -g && \
    NODE_ENV=dev npm install

EXPOSE 3000

CMD ["npm", "run", "start"]