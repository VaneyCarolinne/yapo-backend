import { song } from './dtos/song.dto';
export declare class AppService {
    getHello(): string;
    getItunesMusic(artistName: string): Promise<any>;
    addToFavorites(cancion: song): Promise<string>;
}
