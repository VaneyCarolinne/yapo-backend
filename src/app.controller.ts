import { Body, Controller, Get, Post, Query } from '@nestjs/common';
import { AppService } from './app.service';
import { song } from './dtos/song.dto';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }
  @Get('/search_tracks')
  async searchTracks(@Query('name') name: string) {
    return this.appService.getItunesMusic(name);
  }
  @Post('/favoritos')
  async addToFavorites(@Body() cancion: song) {
    return this.appService.addToFavorites(cancion);
  }
}
