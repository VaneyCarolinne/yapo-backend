"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AppService = void 0;
const common_1 = require("@nestjs/common");
const axios_1 = require("axios");
const fs = require("fs");
let AppService = class AppService {
    getHello() {
        return 'Hello World!';
    }
    async getItunesMusic(artistName) {
        return axios_1.default
            .get('https://itunes.apple.com/search?term=' + artistName, {})
            .then((response) => {
            const data = response.data.results
                .filter((item) => item.artistName.toLowerCase() === artistName.toLowerCase())
                .slice(0, 25);
            let albumes = data.map((item) => item.collectionName);
            albumes = albumes.filter((item, index) => {
                return albumes.indexOf(item) === index;
            });
            const result = {
                total_albumes: albumes.length,
                total_canciones: data.map((item) => item.trackName).length,
                albumes: albumes,
                canciones: data,
            };
            return result;
        })
            .catch((error) => error);
    }
    async addToFavorites(cancion) {
        return axios_1.default
            .get('https://itunes.apple.com/search?term=' + cancion.nombre_banda, {})
            .then((response) => {
            const data = response.data.results.filter((item) => item.artistName.toLowerCase() ===
                cancion.nombre_banda.toLowerCase());
            const result = data.filter((item) => item.trackId == cancion.cancion_id);
            if (result.length > 0) {
                const filename = 'favorities.json';
                const songJson = JSON.stringify(cancion);
                fs.writeFileSync(filename, songJson);
                return songJson;
            }
            else {
                throw new common_1.BadRequestException('Canción Inválida');
            }
        })
            .catch((error) => error);
    }
};
AppService = __decorate([
    (0, common_1.Injectable)()
], AppService);
exports.AppService = AppService;
//# sourceMappingURL=app.service.js.map