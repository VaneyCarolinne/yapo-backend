import { AppService } from './app.service';
import { song } from './dtos/song.dto';
export declare class AppController {
    private readonly appService;
    constructor(appService: AppService);
    getHello(): string;
    searchTracks(name: string): Promise<any>;
    addToFavorites(cancion: song): Promise<string>;
}
